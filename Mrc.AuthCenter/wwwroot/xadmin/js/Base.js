﻿//ajax类型Model
var ajaxType = {
    post: 'post',
    get: 'get'
};
//ajax返回状态码
var ajaxStatus={
    OK:200,
    Fail:500,
    Unauthorized:103
};
var LOCALHOST = "";
function BaseAjax(type, url, data, callback) {
    $.ajax({
        type: type,
        url: LOCALHOST + url,
        data: data,
        dataType: "json",
        success: function (repo) {
            repo = repo.msg !== undefined ? repo : JSON.parse(repo);           
            if (typeof callback === 'function')
            {                         
                layer.msg(repo.msg, { time: 1000 });
                setTimeout(function () { callback(repo);}, 1000);
            }
        },
        error: function (XMLHttpRequest, textStatus, errorThrown) {
            console.log("请求失败：" + url + "data:" + JSON.stringify(data));
        }
    });
}

function showAndReload(layer, repo)
{
    repo = repo.msg !== undefined ? repo : JSON.parse(repo);
    if (repo.status === ajaxStatus.Fail)
    {
        layer.msg(repo.msg);
    }
    else
    {
        layer.msg(repo.msg, {time:1000}, function () {
            window.location.reload();
        });
    }
}


function DateFormat(jsondate) {
    jsondate = jsondate.replace("/Date(", "").replace(")/", "");
    if (jsondate.indexOf("+") > 0) {
        jsondate = jsondate.substring(0, jsondate.indexOf("+"));
    }
    else if (jsondate.indexOf("-") > 0) {
        jsondate = jsondate.substring(0, jsondate.indexOf("-"));
    }
    var date = new Date(parseInt(jsondate, 10));
    var month = date.getMonth() + 1 < 10 ? "0" + (date.getMonth() + 1) : date.getMonth() + 1;
    var currentDate = date.getDate() < 10 ? "0" + date.getDate() : date.getDate();

    return date.getFullYear()
        + "-"
        + month
        + "-"
        + currentDate
        + ""
        + " "
        + date.getHours()
        + ":"
        + date.getMinutes();
}

(function ($) {
    $.fn.extend({
        initForm: function (options) {
            //默认参数
            var defaults = {
                jsonValue: "",
                isDebug: false	//是否需要调试，这个用于开发阶段，发布阶段请将设置为false，默认为false,true将会把name value打印出来
            }
            //设置参数
            var setting = $.extend({}, defaults, options);
            var form = this;
            jsonValue = setting.jsonValue;

            //如果传入的json对象为空，则不做任何操作
            if (!$.isEmptyObject(setting)) {
                var debugInfo = "";
                $.each(setting, function (key, value) {
                    //是否开启调试，开启将会把name value打印出来
                    if (setting.isDebug) {
                        alert("name:" + key + "; value:" + value);
                        debugInfo += "name:" + key + "; value:" + value + " || ";
                    }
                    var formField = form.find("[name='" + key + "']");
                    if ($.type(formField[0]) === "undefined") {
                        if (setting.isDebug) {
                            alert("can not find name:[" + key + "] in form!!!");	//没找到指定name的表单
                        }
                    } else {
                        var fieldTagName = formField[0].tagName.toLowerCase();
                        if (fieldTagName === "input") {
                            if (formField.attr("type") === "radio") {
                                $("input:radio[name='" + key + "']").removeAttr('checked');
                                $("input:radio[name='" + key + "'][value='" + value + "']").attr("checked", "checked");
                            } else {
                                formField.val(value);
                            }
                        } else if (fieldTagName === "select") {
                            //do something special
                            formField.val(value);
                        } else if (fieldTagName === "textarea") {
                            //do something special
                            formField.val(value);
                        } else {
                            formField.val(value);
                        }
                    }
                });
                if (setting.isDebug) {
                    alert(debugInfo);
                }
            }
            return form;	//返回对象，提供链式操作
        }
    });
})(jQuery);

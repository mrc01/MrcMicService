﻿#region 
/*---------------------------------------------------------------- 
* 项目名称 ：DotNetEx.Helpers 
* 项目描述 ： 
* 类 名 称 ：IdHelper 
* 类 描 述 ： 
* 所在的域 ：DESKTOP-HKKGEJ7
* 命名空间 ：DotNetEx.Helpers
* 机器名称 ：DESKTOP-HKKGEJ7 
* CLR 版本 ：4.0.30319.42000 
* 作 者 ：mrc 
* 创建时间 ：2019/2/16 23:08:20 
* 更新时间 ：2019/2/16 23:08:20 
* 版 本 号 ：v1.0.0.0 
******************************************************************* 
* Copyright @ mrc 2019. All rights reserved. *******************************************************************
 //----------------------------------------------------------------*/
#endregion
using System;
using System.Collections.Generic;
using System.Text;

namespace DotNetEx
{
    public static class IdHelper
    {
        public static string CreateGuid()
        {
            Guid id = Guid.NewGuid();
            return id.ToString("N").ToLower();
        }

        public static long CreateSnowflakeId()
        {
            return IdWorker.Instance.NextId();
        }
        public static string CreateStringSnowflakeId()
        {
            return CreateSnowflakeId().ToString();
        }
    }
}

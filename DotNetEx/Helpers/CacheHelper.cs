﻿using Microsoft.Extensions.Caching.Memory;
using System;
using System.Collections.Generic;
using System.Text;

namespace DotNetEx
{
    public abstract partial class CacheHelper
    {
        private static IMemoryCache _memoryCache=null;

        public static IMemoryCache memoryCache
        {
            get
            {
                if(_memoryCache==null)
                {

                }
                return _memoryCache;
            }
            set
            {
                _memoryCache = value;
            }
        }
    }
}

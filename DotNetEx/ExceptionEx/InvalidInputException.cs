﻿#region 
/*---------------------------------------------------------------- 
* 项目名称 ：DotNetEx.ExceptionEx 
* 项目描述 ： 
* 类 名 称 ：InvalidInputException 
* 类 描 述 ： 
* 所在的域 ：DESKTOP-HKKGEJ7
* 命名空间 ：DotNetEx.ExceptionEx
* 机器名称 ：DESKTOP-HKKGEJ7 
* CLR 版本 ：4.0.30319.42000 
* 作 者 ：mrc 
* 创建时间 ：2019/2/17 0:04:40 
* 更新时间 ：2019/2/17 0:04:40 
* 版 本 号 ：v1.0.0.0 
******************************************************************* 
* Copyright @ mrc 2019. All rights reserved. *******************************************************************
 //----------------------------------------------------------------*/
#endregion
using System;
using System.Collections.Generic;
using System.Text;

namespace DotNetEx.ExceptionEx
{
    public class InvalidInputException : Exception
    {
        public InvalidInputException()
        {
        }
        public InvalidInputException(string message)
            : base(message)
        {
        }
    }

    public class MrcException : Exception
    {
        public MrcException()
        {
        }
        public MrcException(string message)
            : base(message)
        {
        }
    }

}

﻿using Surging.Core.Caching;
using Surging.Core.Common;
using Surging.Core.CPlatform.EventBus.Events;
using Surging.Core.CPlatform.Filters.Implementation;
using Surging.Core.CPlatform.Ioc;
using Surging.Core.CPlatform.Runtime.Client.Address.Resolvers.Implementation.Selectors.Implementation;
using Surging.Core.CPlatform.Runtime.Server.Implementation.ServiceDiscovery.Attributes;
using Surging.Core.CPlatform.Support;
using Surging.Core.CPlatform.Support.Attributes;
using Surging.Core.KestrelHttpServer;
using Surging.Core.KestrelHttpServer.Internal;
using Surging.Core.System.Intercept;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Mrc.Entity;
using Mrc.Entities;

namespace Mrc.AuthService.IService
{
    /// <summary>
    /// 登录服务
    /// </summary>
    [ServiceBundle("api/{Service}")]
    public interface IAccountLogonService : IServiceKey
    {
        /// <summary>
        /// 用户登录
        /// </summary>
        /// <param name="loginName">账号</param>
        /// <param name="password">密码</param>
        /// <returns></returns>
        Task<SysUser> CheckLogon(string loginName, string password);
    }
}

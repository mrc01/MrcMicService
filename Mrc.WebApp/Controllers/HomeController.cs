﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Mrc.AppService.IService;
using Mrc.WebApp.Models;
using Surging.Core.Caching.DependencyResolution;
using Surging.Core.CPlatform.Utilities;
using Surging.Core.ProxyGenerator;

namespace Mrc.WebApp.Controllers
{
    public class HomeController : Controller
    {
        private readonly IServiceProxyFactory _serviceProxyFactory;
        private readonly ISysFilterIPService _sysFilterIPService;
        public HomeController(IServiceProxyFactory serviceProxyFactory)
        {
            _serviceProxyFactory = serviceProxyFactory;
            _sysFilterIPService = _serviceProxyFactory.CreateProxy<ISysFilterIPService>("SysFilterIP");
        }
        public IActionResult Index()
        {

            return View();
        }
        public IActionResult Index2()
        {

            return View();
        }
       // [Login]
        public async  Task<JsonResult> GetData()
        {
            //var sysIpFilterProxy = ServiceLocator.GetService<IServiceProxyFactory>().CreateProxy<ISysFilterIPService>("SysFilterIP");
            //var a = await sysIpFilterProxy.GetSingle("1098606624219729921");
            //var userProxy = _serviceProxyFactory.CreateProxy<ISysFilterIPService>("SysFilterIP");
            var e1 = await _sysFilterIPService.GetList();
            var e2 = await _sysFilterIPService.GetList();
            var e3 = await _sysFilterIPService.GetList();
            var e4 = await _sysFilterIPService.GetList();
            var e5 = await _sysFilterIPService.GetList();
            var e6 = await _sysFilterIPService.GetList();
            var e7 = await _sysFilterIPService.GetList();
            var e8 = await _sysFilterIPService.GetList();
            var e9 = await _sysFilterIPService.GetList();
            return Json(e9);
        }

        public IActionResult Privacy()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}

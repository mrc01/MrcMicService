﻿using Mrc.AppService.IService;
using Surging.Core.ProxyGenerator;
using System;
using System.Collections.Generic;
using System.Text;
using Chloe;
using Mrc.Entity;
using System.Threading.Tasks;
using Surging.Core.CPlatform.Ioc;
using Mrc.DBContext;

namespace Mrc.AppService
{
    /// <summary>
    /// ip过滤
    /// </summary>
    [ModuleName("SysFilterIP")]
    public class SysFilterIPService: ProxyServiceBase,ISysFilterIPService
    {
        /// <summary>
        /// 获取单条
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public async Task<SysFilterIP> GetSingle(string id)
        {
            return await Task.Run(() =>
            {
                using (var thisDbContext = DbContextHelper.GetDBContext())
                {
                    return thisDbContext.QueryByKey<SysFilterIP>(id);
                };            
            });
        }
        /// <summary>
        /// 获取list
        /// </summary>
        /// <returns></returns>
        public async Task<List<string>> GetList()
        {
            return await Task.Run(() =>
            {
                using (var thisDbContext = DbContextHelper.GetDBContext())
                {
                    return thisDbContext.Query<SysFilterIP>().Where(x=>x.Id!=string.Empty).Select(x=>x.StartIP).ToList();
                };
            });
        }
    }
}

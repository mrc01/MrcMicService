﻿using Surging.Core.Caching;
using Surging.Core.Common;
using Surging.Core.CPlatform.EventBus.Events;
using Surging.Core.CPlatform.Filters.Implementation;
using Surging.Core.CPlatform.Ioc;
using Surging.Core.CPlatform.Runtime.Client.Address.Resolvers.Implementation.Selectors.Implementation;
using Surging.Core.CPlatform.Runtime.Server.Implementation.ServiceDiscovery.Attributes;
using Surging.Core.CPlatform.Support;
using Surging.Core.CPlatform.Support.Attributes;
using Surging.Core.KestrelHttpServer;
using Surging.Core.KestrelHttpServer.Internal;
using Surging.Core.System.Intercept;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Mrc.Entity;
namespace Mrc.AppService.IService
{
    /// <summary>
    /// ip过滤服务
    /// </summary>
    [ServiceBundle("api/{Service}")]   
    public interface ISysFilterIPService:IServiceKey
    {
        /// <summary>
        /// 获取单条数据
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [Command(Strategy = StrategyType.Injection, ShuntStrategy = AddressSelectorMode.HashAlgorithm, ExecutionTimeoutInMilliseconds = 2500, BreakerRequestVolumeThreshold = 3, Injection = @"return null;", RequestCacheEnabled = false)]
        Task<SysFilterIP> GetSingle(string id);

        /// <summary>
        /// 获取列表
        /// </summary>
        /// <returns></returns>
        [Command(Strategy = StrategyType.Injection, ShuntStrategy = AddressSelectorMode.HashAlgorithm, ExecutionTimeoutInMilliseconds = 2500, BreakerRequestVolumeThreshold = 3, Injection = @"return null;", RequestCacheEnabled = false)]
        Task<List<string>> GetList();

    }
}

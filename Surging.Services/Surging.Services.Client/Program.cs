﻿using Autofac;
using Microsoft.Extensions.Logging;
using Surging.Core.Caching;
using Surging.Core.Caching.Configurations;
using Surging.Core.Codec.MessagePack;
using Surging.Core.Consul;
using Surging.Core.Consul.Configurations;
using Surging.Core.CPlatform;
using Surging.Core.CPlatform.Configurations;
using Surging.Core.CPlatform.DependencyResolution;
using Surging.Core.CPlatform.Utilities;
using Surging.Core.DotNetty;
using Surging.Core.EventBusRabbitMQ;
using Surging.Core.EventBusRabbitMQ.Configurations;
using Surging.Core.Log4net;
using Surging.Core.Nlog;
using Surging.Core.ProxyGenerator;
using Surging.Core.ServiceHosting;
using Surging.Core.ServiceHosting.Internal.Implementation;
using Surging.Core.System.Intercept;
using Surging.IModuleServices.Common;
using System;
using System.Diagnostics;
//using Surging.Core.Zookeeper;
//using Surging.Core.Zookeeper.Configurations;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Mrc.AppService.IService;
namespace Surging.Services.Client
{
    public class Program
    {
        private static int _endedConnenctionCount = 0;
        private static DateTime begintime;
        static void Main(string[] args)
        {
            Encoding.RegisterProvider(CodePagesEncodingProvider.Instance);
            var host = new ServiceHostBuilder()
                .RegisterServices(builder =>
                {
                    builder.AddMicroService(option =>
                    {
                        option.AddClient()
                        .AddCache();
                        builder.Register(p => new CPlatformContainer(ServiceLocator.Current));
                    });
                })
                .ConfigureLogging(logger =>
                {
                    logger.AddConfiguration(
                        Core.CPlatform.AppConfig.GetSection("Logging"));
                })
                .Configure(build =>
                build.AddEventBusFile("eventBusSettings.json", optional: false))
                .Configure(build =>
                build.AddCacheFile("cacheSettings.json", optional: false, reloadOnChange: true))
                .Configure(build =>
                build.AddCPlatformFile("${surgingpath}|surgingSettings.json", optional: false, reloadOnChange: true))
                .UseClient()
                .UseProxy()
                .UseStartup<Startup>()
                .Build();
                using (host.Run())
                {
                    var sw = new Stopwatch();
                    sw.Start();
                    var sysIpFilterProxy = ServiceLocator.GetService<IServiceProxyFactory>().CreateProxy<ISysFilterIPService>("SysFilterIP");
                    ServiceResolver.Current.Register("SysFilterIP", sysIpFilterProxy);
                    var service = ServiceLocator.GetService<IServiceProxyFactory>();
                    sysIpFilterProxy = ServiceResolver.Current.GetService<ISysFilterIPService>("SysFilterIP");
                    sw.Stop();
                    Console.WriteLine($"代理所花{sw.ElapsedMilliseconds}ms");
                    Task.Run(() => StartRequest(sysIpFilterProxy));
                    for (int i = 0; i < 100; i++)
                    {
                        Task.Run(() => TestAll(sysIpFilterProxy));
                    }
                    Console.ReadLine();
                }
        }
        private static async Task StartRequest(ISysFilterIPService sysIpFilterProxy)
        {
            var sw1 = new Stopwatch();
            sw1.Start();
            for (int i = 0; i < 1000; i++)
            {            
                var a = await sysIpFilterProxy.GetSingle("1098606624219729921");
            }
            sw1.Stop();
            Console.WriteLine($"1000次请求数据时间:{sw1.ElapsedMilliseconds}ms");
        }
        private static async Task TestAll(ISysFilterIPService sysIpFilterProxy)
        {
            var list = await sysIpFilterProxy.GetList();
            var str = string.Join(",", list);
            Console.WriteLine(str);
        }
        public static async Task TestSysFilterIP(ISysFilterIPService sysIpFilterProxy, int connectionCount)
        {
            var a = await sysIpFilterProxy.GetSingle("1098606624219729921");
            Console.WriteLine(a.StartIP);
            IncreaseSuccessConnection(connectionCount);
        }
        private static void IncreaseSuccessConnection(int connectionCount)
        {
            Interlocked.Increment(ref _endedConnenctionCount);
            if (_endedConnenctionCount == 1)
                begintime = DateTime.Now;
            if (_endedConnenctionCount >= connectionCount)
                Console.WriteLine($"结束时间{(DateTime.Now - begintime).TotalMilliseconds}");
        }
    }
}

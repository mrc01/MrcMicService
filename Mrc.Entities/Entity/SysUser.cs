﻿
using Mrc.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Mrc.Entity
{

    public class SysUser:BaseEntity
    {
        public string AccountName { get; set; }
        public string Name { get; set; }
        public string HeadIcon { get; set; }
        public int? Gender { get; set; }
        public DateTime? Birthday { get; set; }
        public string MobilePhone { get; set; }
        public string Email { get; set; }
        public string WeChat { get; set; }
        public string Description { get; set; }
        public DateTime CreateTime { get; set; }
        public string CreateUserId { get; set; }
        public DateTime? LastModifyTime { get; set; }
        public string LastModifyUserId { get; set; }
        public AccountState State { get; set; } = AccountState.Normal;
        /// <summary>
        /// 以 , 分隔
        /// </summary>
        public string OrgIds { get; set; }
        public string PostIds { get; set; }
        public string RoleIds { get; set; }
    }

    public class AddUserInput : SysUser
    {
        public string Password { get; set; }
    }
    public class UpdateUserPwdInput
    {
        public string id { get; set; }
        public string password { get; set; }
        public string cpassword { get; set; }
    }
}

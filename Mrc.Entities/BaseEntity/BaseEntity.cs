﻿using DotNetEx;
using System;
using System.Collections.Generic;
using System.Text;

namespace Mrc.Entity
{
    public class BaseEntity
    {
        public string Id { get; set; }
        public DateTime? DeleteTime { get; set; }
        public string DeleteUserId { get; set; }
        public bool? IsDeleted { get; set; }
        public BaseEntity()
        {
            Id = IdHelper.CreateStringSnowflakeId();
            IsDeleted = false;
        }
    }
}

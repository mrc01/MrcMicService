﻿namespace Mrc.Entity
{
    public class ParentBaseEntity:BaseEntity
    {
        public string ParentId { get; set; }
        public string Name { get; set; }
        public int? Type { get; set; }
    }
}
